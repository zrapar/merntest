import React from 'react';
import Moment from 'react-moment';
import PropTypes from 'prop-types';

const ProfileExperience = ({ profile: { experience } }) => {
    return (
        <div className='profile-exp bg-white p-2'>
            <h2 className='text-primary'>Experience</h2>
            {experience && experience.length > 0 ? (
                experience.map((exp, index) => (
                    <div key={index}>
                        <h3 className='text-dark'>{exp.company}</h3>
                        <p>
                            <Moment format='MMM, YYYY'>{exp.from}</Moment>{' '}
                            {!exp.to ? (
                                ' - Now'
                            ) : (
                                <Moment format='MMM, YYYY'>{exp.to}</Moment>
                            )}
                        </p>
                        {exp.current && <small>Current Job</small>}
                        <p>
                            <strong>Position: </strong>
                            {exp.title}
                        </p>
                        <p>
                            <strong>Description: </strong>
                            {exp.description}
                        </p>
                    </div>
                ))
            ) : (
                <h4>No Experience Credentials</h4>
            )}
        </div>
    );
};

ProfileExperience.propTypes = {
    profile: PropTypes.object.isRequired
};

export default ProfileExperience;
