import React, { Fragment } from 'react';
import Moment from 'react-moment';
import PropTypes from 'prop-types';

const ProfileEducation = ({ profile: { education } }) => {
    return (
        <Fragment>
            <div className='profile-edu bg-white p-2'>
                <h2 className='text-primary'>Education</h2>
                {education && education.length > 0 ? (
                    education.map((edu, index) => (
                        <div key={index}>
                            <h3 className='text-dark'>{edu.school}</h3>
                            <p>
                                <Moment format='MMM, YYYY'>{edu.from}</Moment>{' '}
                                {!edu.to ? (
                                    ' - Now'
                                ) : (
                                    <Moment format='MMM, YYYY'>{edu.to}</Moment>
                                )}
                            </p>
                            {edu.current && <small>Currently</small>}
                            <p>
                                <strong>Degree: </strong>
                                {edu.degree}
                            </p>
                            <p>
                                <strong>Field Of Study: </strong>
                                {edu.fieldofstudy}
                            </p>
                            <p>
                                <strong>Description: </strong>
                                {edu.description}
                            </p>
                        </div>
                    ))
                ) : (
                    <h4>No Education Credentials</h4>
                )}
            </div>
        </Fragment>
    );
};

ProfileEducation.propTypes = {
    profile: PropTypes.object.isRequired
};

export default ProfileEducation;
