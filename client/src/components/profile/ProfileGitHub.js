import React, { useEffect, Fragment } from 'react';
import PropTypes from 'prop-types';
import Spinner from '../layouts/Spinner';
import { connect } from 'react-redux';
import { getGitHubRepos } from '../../actions/profile';

const ProfileGitHub = ({
    profile: { githubusername },
    getGitHubRepos,
    repos
}) => {
    useEffect(() => {
        getGitHubRepos(githubusername);
    }, [getGitHubRepos, githubusername]);

    return (
        <Fragment>
            {githubusername && (
                <div className='profile-github'>
                    <h2 className='text-primary my-1'>
                        <i className='fab fa-github' /> Github Repos
                    </h2>
                    {repos === null ? (
                        <Spinner />
                    ) : repos.length > 0 ? (
                        repos.map(repo => (
                            <div
                                key={repo.id}
                                className='repo bg-white p-1 my-1'
                            >
                                <div>
                                    <h4>
                                        <a
                                            href={repo.html_url}
                                            target='_blank'
                                            rel='noopener noreferrer'
                                        >
                                            {repo.name}
                                        </a>
                                    </h4>
                                    <p>{repo.description}</p>
                                </div>
                                <div>
                                    <ul>
                                        <li className='badge badge-primary'>
                                            Stars: {repo.stargazers_count}
                                        </li>
                                        <li className='badge badge-dark'>
                                            Watchers: {repo.watchers_count}
                                        </li>
                                        <li className='badge badge-light'>
                                            Forks: {repo.forks_count}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        ))
                    ) : (
                        <h4> No github Repos</h4>
                    )}
                </div>
            )}
        </Fragment>
    );
};

ProfileGitHub.propTypes = {
    profile: PropTypes.object.isRequired,
    getGitHubRepos: PropTypes.func.isRequired,
    repos: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
    repos: state.profile.repos
});

export default connect(
    mapStateToProps,
    { getGitHubRepos }
)(ProfileGitHub);
