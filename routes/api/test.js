const express = require('express');
const router = express.Router();
const { localidades } = require('../../data/localidades.json');
const { partidos } = require('../../data/partidos.json');
const _ = require('lodash');
const fetch = require('node-fetch');
const mapLimit = require('async/mapLimit');
const fs = require('fs');

// @route      GET api/test
// @desc       test
// @access     Public

router.get('/', async (req, res) => {
	try {
		const url = 'https://apis.datos.gob.ar/georef/api/localidades?nombre=';
		let index = 0;

		const urls = _.map(localidades, (value) => {
			return {
				fullUrl      : `${url}${encodeURI(value['localidad'])}`,
				nameInSearch : value['localidad']
			};
		});
		const current_datetime = new Date();
		let formatted_date =
			current_datetime.getFullYear() +
			'-' +
			(current_datetime.getMonth() + 1) +
			'-' +
			current_datetime.getDate() +
			'-' +
			current_datetime.getHours() +
			'-' +
			current_datetime.getMinutes() +
			'-' +
			current_datetime.getSeconds();

		const finalLength = urls.length;

		const showMeIndex = (num) => {
			const progress = `${num} of ${finalLength} \n`;
			fs.appendFile(`progress-at-${formatted_date}.txt`, progress, function(err) {
				if (err) throw err;
			});
		};

		mapLimit(
			urls,
			1,
			async function({ fullUrl, nameInSearch }) {
				try {
					let response = await fetch(fullUrl);
					let data = await response.json();

					if (
						data.cantidad > 0 &&
						data.localidades[0].hasOwnProperty('departamento') &&
						data.localidades[0].departamento.hasOwnProperty('nombre')
					) {
						return {
							urlRequested : fullUrl,
							name         : nameInSearch,
							data         : data.localidades[0].departamento.nombre,
							success      : true,
							reason       : null
						};
					} else {
						return {
							name         : nameInSearch,
							urlRequested : fullUrl,
							data         : data.cantidad > 0 ? data.localidades[0] : null,
							success      : false,
							reason       : 'noData'
						};
					}
				} catch (err) {
					console.log(err);
					return {
						name         : nameInSearch,
						urlRequested : fullUrl,
						data         : null,
						success      : false,
						reason       : 'onRequest'
					};
				} finally {
					index++;
					showMeIndex(index);
				}
			},
			(err, results) => {
				if (err) throw err;

				const nameWithResultsJson = `withResults-at-${formatted_date}.json`;
				const nameWithNoResultsJson = `withNoResults-at-${formatted_date}.json`;
				const departamentosName = `departamentos-partidos-con-localidades-at-${formatted_date}.json`;
				const differenceName = `difference-departamento-partidosNames-at-${formatted_date}.json`;
				const withResults = results.filter((i) => i.success);
				const withNoResults = results.filter((i) => !i.success);
				const founded = withResults.map((i) => i.data);
				const uniqueArray = [ ...new Set(founded) ];
				const departamentosPartidosWithLocalidades = uniqueArray.map((o, index) => {
					const findData = withResults.filter((i) => i.data === o);
					const findId = partidos.filter((i) => encodeURI(i.nombre) === encodeURI(o));
					return {
						[o] : {
							cant                    : findData.length,
							data                    : findData,
							id_partido_departamento :
								findId.length > 0
									? findId[0].id
									: {
											reason : 'noData',
											oName  : o,
											index  : index
										}
						}
					};
				});

				const allDepartamentoPartidoNames = partidos.map((i) => i.nombre);
				const differenceDepartamentoPartidosNames = allDepartamentoPartidoNames
					.filter((x) => !uniqueArray.includes(x))
					.concat(uniqueArray.filter((x) => !allDepartamentoPartidoNames.includes(x)))
					.map((o) => {
						if (o) {
							const data = partidos.filter((i) => encodeURI(i.nombre) === encodeURI(o));
							return {
								id           : data[0].id,
								nombre       : data[0].nombre,
								categoria    : data[0].categoria,
								id_provincia : data[0].id_provincia
							};
						} else {
							return false;
						}
					})
					.filter((i) => i);
				fs.appendFile(
					nameWithResultsJson,
					JSON.stringify(
						{
							cant : withResults.length,
							data : withResults
						},
						null,
						2
					),
					function(err) {
						if (err) throw err;
						console.log(`${nameWithResultsJson} created`);
					}
				);
				fs.appendFile(
					nameWithNoResultsJson,
					JSON.stringify(
						{
							cant : withNoResults.length,
							data : withNoResults
						},
						null,
						2
					),
					function(err) {
						if (err) throw err;
						console.log(`${nameWithNoResultsJson} created`);
					}
				);
				fs.appendFile(
					departamentosName,
					JSON.stringify(
						{
							cant : departamentosPartidosWithLocalidades.length,
							data : departamentosPartidosWithLocalidades
						},
						null,
						2
					),
					function(err) {
						if (err) throw err;
						console.log(`departamentosPartidosWithLocalidades json created`);
					}
				);
				fs.appendFile(differenceName, JSON.stringify(differenceDepartamentoPartidosNames, null, 2), function(
					err
				) {
					if (err) throw err;
					console.log(`differenceName json created`);
				});
			}
		);

		res.status(200).send('Process Started V3');
	} catch (error) {
		console.log(error);
		console.error('aqui2', error.message);
		res.status(500).send('Server Error');
	}
});

module.exports = router;

// .map((o) => {
//         const data = partidos.filter((i) => encodeURI(i.nombre) === encodeURI(o));

// 			});
